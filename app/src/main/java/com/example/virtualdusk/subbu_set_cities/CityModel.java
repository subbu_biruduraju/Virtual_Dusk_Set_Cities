package com.example.virtualdusk.subbu_set_cities;

import java.util.List;

public class CityModel {

    public List<City> Cities = null;
    public String Status;

    public class City {
        public String City_Id;
        public String City_Id_Name;
        public String City_Name;
        public String City_Title;
        public String City_Main_Image;
        public String City_Image;
        public String City_Description;
    }

}


