package com.example.virtualdusk.subbu_set_cities;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.EditText;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.google.gson.Gson;

import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity {

    private CitiesAdapter mCitiesAdapter;
    private List<String> mCityList = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        EditText editTextSearchCity = findViewById(R.id.edit_text_search);
        editTextSearchCity.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                if (editable.length() < 1) {
                    if (mCityList.size() > 0) {
                        mCitiesAdapter.setCitiesData(mCityList);
                    }
                } else {
                    List<String> filteredList = new ArrayList<>();
                    for (String cityName : mCityList) {
                        if (cityName.toLowerCase().contains(editable.toString().toLowerCase())) {
                            filteredList.add(cityName);
                        }
                    }
                    mCitiesAdapter.setCitiesData(filteredList);
                }
            }
        });
        final RecyclerView recyclerViewCities = findViewById(R.id.recycler_view_cities);
        recyclerViewCities.requestFocus();
        recyclerViewCities.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false));
        mCitiesAdapter = new CitiesAdapter();
        recyclerViewCities.setAdapter(mCitiesAdapter);

        findViewById(R.id.llayout).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                recyclerViewCities.requestFocus();
            }
        });
        getCitiesList();
    }

    private void getCitiesList() {
        String url = "http://13.126.151.196/json.php?action=newCmsPlacesInterview";
        RequestQueue queue = Volley.newRequestQueue(this);
        final StringRequest request = new StringRequest(Request.Method.POST, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        CityModel cityModel = new Gson().fromJson(response, CityModel.class);
                        List<CityModel.City> citiesModelList = cityModel.Cities;
                        for (CityModel.City city : citiesModelList) {
                            mCityList.add(city.City_Title);
                        }
                        mCitiesAdapter.setCitiesData(mCityList);
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        System.out.print(error.getMessage());
                    }
                }
        ) {
            @Override
            public byte[] getBody() {
                RequestObject requestObject = new RequestObject();
                requestObject.Lan_Id = 6;
                return new Gson().toJson(requestObject).getBytes();
            }

        };
        queue.add(request);
    }

    public class RequestObject {
        public int Lan_Id;
    }
}
