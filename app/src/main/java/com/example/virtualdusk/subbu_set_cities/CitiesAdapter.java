package com.example.virtualdusk.subbu_set_cities;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

public class CitiesAdapter extends RecyclerView.Adapter<CitiesAdapter.ViewHolder> {

    public List<String> mCitiesList = new ArrayList<>();

    public void setCitiesData(List<String> list) {
        mCitiesList.clear();
        mCitiesList.addAll(list);
        notifyDataSetChanged();
    }

    @NonNull
    @Override
    public CitiesAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new ViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.item_city, parent, false));
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        String cityName = mCitiesList.get(position);
        holder.textViewCity.setText(cityName);
    }

    @Override
    public int getItemCount() {
        return mCitiesList.size();
    }

    class ViewHolder extends RecyclerView.ViewHolder {

        TextView textViewCity;

        ViewHolder(View itemView) {
            super(itemView);
            textViewCity = itemView.findViewById(R.id.text_view_city_name);
        }
    }
}
